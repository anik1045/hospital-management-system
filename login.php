<?php
	
	include('login_action.php');

?>

<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
		<meta charset="utf-8">
		<link href="frontend/login/css/style.css" rel='stylesheet' type='text/css' />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
		<!--webfonts-->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:600italic,400,300,600,700' rel='stylesheet' type='text/css'>
		<!--//webfonts-->
</head>
<body>
	<div class="main">
		<div class="login-form">
			<h1>Member Login</h1>
					<div class="head">
						<img src="frontend/login/images/user.png" alt=""/>
					</div>

	<form method="post" action:"login_action.php">
		
		<div class="input-group">
			<label>username</label>
			<input type="text" name="username">
		</div>

		
		
		<div class="input-group">
			<label>password</label>
			<input type="password" name="pass">
		</div>

		
		<div class="input-group">
			<button type="submit" name="log" class="btn">Login</button>	
		</div>
		<p>
			Not already member? <a href="reg_form.php">Sign up</a>
		</p>
	</form>

</body>
</html>
 